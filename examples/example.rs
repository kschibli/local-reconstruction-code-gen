use local_reconstruction_code_gen::gen_encode_matrix;
g2p::g2p!(GF16, 4, modulus: 0b10011);

fn main() {
    let k = 12;
    let l = 2;
    let r = 2;

    let encode_matrix = gen_encode_matrix::<GF16>(k, l, r)
        .unwrap()
        .collect::<Vec<_>>();

    for row in encode_matrix.chunks_exact(k) {
        println!();
        for val in row {
            print!("{:#0width$b}, ", val, width = 10)
        }
    }
}
