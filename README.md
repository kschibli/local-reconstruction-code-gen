# local-reconstruction-code-gen

This crate provides a function that generates encode matrices for Local Reconstruction Codes as described in the paper
[Erasure Coding in Windows Azure Storage][1] by Huang et al.

The implementation only supports values of `r` of size up to (including) 2.

## Example

This example shows how to create an encode matrix for a (6,2,2) Local Reconstruction Code. You must specify an
an implementation of the [`GaloisField`][2] trait as a type argument. Both the trait and macros to generate types
are found in the [`g2p`][3] crate.

```rust
use local_reconstruction_code_gen::gen_encode_matrix;

g2p::g2p!(GF16, 4, modulus: 0b10011);

fn example() {
    let encode_matrix = gen_encode_matrix::<GF16>(k, l, r)
        .unwrap()
        .collect::<Vec<_>>();
}
```

[1]: https://www.usenix.org/conference/atc12/technical-sessions/presentation/huang
[2]: https://docs.rs/g2p/0.4/g2p/trait.GaloisField.html
[3]: https://crates.io/crates/g2p